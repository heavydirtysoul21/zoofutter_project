import Models.*;
import Models.Zoo;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();


        Mitarbeiter mitarbeiter1 = new Mitarbeiter("1", "Joe Frie");
        Mitarbeiter mitarbeiter2 = new Mitarbeiter("2", "Lisa Schmidt");
        Futter futter1 = new Futter("Fleisch", "kg Fleisch");
        Futter futter2 = new Futter("Fisch", "kg fisсh");
        Futter futter3 = new Futter("Banane", "kg banana");

        Vorratsposten vorratsposten1 = new Vorratsposten(futter1, 1);
        Vorratsposten vorratsposten2 = new Vorratsposten(futter2, 1);
        Vorratsposten vorratsposten3 = new Vorratsposten(futter3, 1);

        zoo.addFutterLagerbestand(Arrays.asList(vorratsposten1, vorratsposten2, vorratsposten3));

        HashMap<Futter, Integer> lionFutter = new HashMap<Futter, Integer>() {{
            put(futter1, 25);
        }};
        HashMap<Futter, Integer> eisbaerFutter = new HashMap<Futter, Integer>() {{
            put(futter1, 20);
            put(futter2, 14);
        }};

        HashMap<Futter, Integer> affeFutter = new HashMap<Futter, Integer>() {{
            put(futter3, 4);
        }};
        HashMap<Futter, Integer> pinguinFutter = new HashMap<Futter, Integer>() {{
            put(futter2, 2);
        }};

        Tier tier1 = new Tier("Lion", lionFutter);
        Tier tier2 = new Tier("Affe", affeFutter);
        Tier tier3 = new Tier("Pinguin", pinguinFutter);
        Tier tier4 = new Tier("Eisbaer", eisbaerFutter);

        zoo.addTieren(Arrays.asList(tier1, tier2, tier3, tier4));

        zoo.berechtigungErteilen(mitarbeiter1);

        System.out.println("Gesamt Futterbedarf pro Tag:");
        zoo.berechneGesamtfutterbedarf().forEach(((futter, integer) -> System.out.println(futter.getName() + " : " + integer + " " + futter.getBeschreibung())));

        zoo.fehlendesFutterBestellen(mitarbeiter1).forEach(((futter, integer) -> System.out.println(futter.getName()+" : "+integer)));


    }
}
