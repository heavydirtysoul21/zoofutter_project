package Models;

public class Vorratsposten {
    //складские запасы
    private int lagerbestand;
    private Futter futter;

    //Кладовые
    public Vorratsposten(Futter futter, int lagerbestand) {
        this.lagerbestand = lagerbestand;
        this.futter = futter;
    }

    public Futter getFutter() {
        return futter;
    }

    public void setFutter(Futter futter) {
        this.futter = futter;
    }

    public void setLagerbestand(int lagerbestand) {
        this.lagerbestand = lagerbestand;
    }

    public int getLagerbestand() {
        return lagerbestand;
    }

}