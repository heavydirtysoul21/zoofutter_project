package Models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Bestellung {
	//общая цена
	private double gesamtpreis;
	public ArrayList<Bestellposten> bestellpostenList = new ArrayList<>();
	public Bestellung() {

	}
	//Добавить почту заказа
	public void bestellpostenHinzufuegen(Bestellposten bestellposten) {
		bestellpostenList.add(bestellposten);
	}

	public double getGesamtpreis() {
		return gesamtpreis;
	}

	public void setGesamtpreis(double gesamtpreis) {
		this.gesamtpreis = gesamtpreis;
	}
}