package Models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Zoo {
    private ArrayList<Mitarbeiter> berechtigteMitarbeiter = new ArrayList<>();
    private ArrayList<Vorratsposten> lagerbestand = new ArrayList<>();
    private ArrayList<Tier> tiereImZoo = new ArrayList<>();

    public Zoo() {
    }

    public ArrayList<Mitarbeiter> getBerechtigteMitarbeiter() {
        return berechtigteMitarbeiter;
    }

    public ArrayList<Tier> getTiereImZoo() {
        return tiereImZoo;
    }

    public ArrayList<Vorratsposten> getLagerbestand() {
        return lagerbestand;
    }

    public void addFutterLagerbestand(List<Vorratsposten> futterList) {
        lagerbestand.addAll(futterList);
    }

    public void addTieren(List<Tier> tierList) {
        tiereImZoo.addAll(tierList);
    }

    //предоставление разрешения
    public void berechtigungErteilen(Mitarbeiter mitarbeiter) {
        if (!berechtigteMitarbeiter.contains(mitarbeiter)) {
            berechtigteMitarbeiter.add(mitarbeiter);
        } else {
            System.out.println(mitarbeiter.getName() + " schon hat Berechtigung");
        }
    }

    //отозвать разрешение
    public void berechtigungZurueckziehen(Mitarbeiter mitarbeiter) {
        if (berechtigteMitarbeiter.contains(mitarbeiter)) {
            berechtigteMitarbeiter.remove(mitarbeiter);
        } else {
            System.out.println(mitarbeiter.getName() + " schon hat keine Berechtigung");
        }
    }

    //расходование припасов
    public void vorraeteAusgeben() {

    }

    //рассчитайте общую потребность в корме
    public HashMap<Futter, Integer> berechneGesamtfutterbedarf() {
        HashMap<Futter, Integer> gesamtFutterBedarf = new HashMap<>();
        for (Tier tier : tiereImZoo) {
            tier.getFutterBedarf().forEach((k, v) -> {
                if (gesamtFutterBedarf.containsKey(k)) {
                    gesamtFutterBedarf.put(k, v + gesamtFutterBedarf.get(k));
                } else {
                    gesamtFutterBedarf.put(k, v);
                }
            });
        }

        return gesamtFutterBedarf;
    }

    //Отсутствующие заказы на корм
    public HashMap<Futter, Integer> fehlendesFutterBestellen(Mitarbeiter mitarbeiter) {
        if (berechtigteMitarbeiter.contains(mitarbeiter)) {
            HashMap<Futter, Integer> gesamtFutterBedarf = berechneGesamtfutterbedarf();
            HashMap<Futter, Integer> fehlendesFutterBedarf = new HashMap<>();

            lagerbestand.forEach((vorratsposten) -> {
                Futter k = vorratsposten.getFutter();
                int v = vorratsposten.getLagerbestand();
                if (gesamtFutterBedarf.containsKey(k)) {
                    fehlendesFutterBedarf.put(k, v - gesamtFutterBedarf.get(k));
                } else {
                    fehlendesFutterBedarf.put(k, v);
                }
            });

            return fehlendesFutterBedarf;
        } else {
            System.out.println(mitarbeiter.getName() + " hat keine Berechtigung zu Bestellen");
        }

        return new HashMap<>();
    }

    //Написать файл заказа
    private void bestellunglnDatieSchreiben() {

        try {
            File bestellungDatei = new File("bestellung.txt");
            if (bestellungDatei.createNewFile()) {
                System.out.println("Datei erstellt: " + bestellungDatei.getName());
            } else {
                System.out.println("Datei existiert bereits");
            }
        } catch (IOException e) {
            System.out.println("Ein Fehler ist aufgetreten");
            e.printStackTrace();
        }
        try {
            FileWriter myWriter = new FileWriter("bestellung.txt");
            myWriter.write("");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("Ein Fehler ist aufgetreten");
            e.printStackTrace();
        }
    }

}