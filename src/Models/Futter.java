package Models;

import java.util.HashMap;

public class Futter extends HashMap<Futter, Integer> {

    private String name;
    private String beschreibung;

    public Futter(String name, String beschreibung) {
        this.name = name;
        this.beschreibung = beschreibung;
    }
    public String getName(){
        return name;
    }
    public String getBeschreibung(){
        return beschreibung;
    }
    @Override
    public int hashCode() {
        int hash = name.hashCode();
        return hash;
    }
}