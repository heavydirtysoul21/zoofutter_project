package Models;

public class Bestellposten {
    //количество
    private int anzahl;
    //цена
    private double preis;

    private Futter futter;
    //Пункт заказа
    public Bestellposten(Futter futter, int anzahl, double preis) {
        this.anzahl = anzahl;
        this.preis = preis;
        this.futter = futter;
    }

    public Futter getFutter() {
        return futter;
    }

    public double getPreis() {
        return preis;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setFutter(Futter futter) {
        this.futter = futter;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

}