package Models;

import java.util.HashMap;

public class Tier {

    private final String name;
    //Потребности в корме
    private HashMap<Futter, Integer> futterBedarf;


    public Tier(String name, HashMap<Futter, Integer> futterBedarf) {
        this.name = name;
        this.futterBedarf = futterBedarf;
    }

    public String getName() {
        return name;
    }

    public HashMap<Futter, Integer> getFutterBedarf(){
        return futterBedarf;
    }


    //Дополнительные потребности в корме
    public void ergaenzeFutterbedarf(Futter futter, int menge) {
        if (futterBedarf.containsKey(futter)){
            futterBedarf.put(futter, menge + futterBedarf.get(futter));
        }
        else{
            futterBedarf.put(futter, menge);
        }
    }


}